# base image
FROM node:12.18.4-alpine3.9

RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app

RUN npm install -D

COPY . /app
EXPOSE 5000

CMD ["npm","start"]