import path from 'path';
import {unlink} from 'fs-extra';
import fs from 'fs';
import {google} from 'googleapis';
import secrets from './../secret';
import Recaudaciones from '../models/recaudaciones';
import Programa from '../models/programa';

const scopes = ['https://www.googleapis.com/auth/drive'];
const auth = new google.auth.JWT(secrets.drive.client_email,null,secrets.drive.private_key,scopes);
const drive = google.drive({version: 'v3', auth});

const ROOT_DIRECTORY = '1n6Nm8YDuAlFEg00WKOL9Wab64qsFoKNP';

function createFolder(numero,idParent){
    return new Promise( (resolve,reject)=>{
        var fileMetadataFolder = {'name': numero, 'mimeType': 'application/vnd.google-apps.folder', 'parents':[`${idParent}`]}
        drive.files.create({resource: fileMetadataFolder, fields:'id'}, function(err,file){
            if(err){
                reject(err);
            } else {
                resolve(file.data.id);
            }
        });
    } );
}

function createFile(fileName,fileLocation,fileMimeType,folderID){
    return new Promise((resolve,reject)=>{
        let fileMetadata= {name: fileName,parents:[folderID]}
        let media = { mimeType: fileMimeType, body: fs.createReadStream(path.join(__dirname,'..','uploads','files',fileLocation))}
        drive.files.create({resource:fileMetadata,media:media,fields:'id'},async function(err,file){
            if(err){
                reject(err);
            } else {
                resolve(file.data.id);
            }
        });
    });
}

export async function editFile(req,res){
    let media = { mimeType: req.file.mimeType, body: fs.createReadStream(path.join(__dirname,'..','uploads','files',req.file.filename))}
    drive.files.update({ fileId: req.body.voucherID, media}, async function(err,file){
        if(err){
            return res.json({err: 'Algo no salió bien'});
        } else {
            unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
            return res.json({status:'ok'});
        }
    });
}

export async function deleteFile(req,res){
    await Recaudaciones.update({id_drive_google:null}, {returning: false,where: {numero:req.body.idRec}});
    drive.files.delete({fileId: req.body.recipeID}, async function(err, file){
        if(err){
            return res.json({err: 'Algo no salió bien'});
        } else {
            return res.json({status:'ok'});
        }
    })
}

export async function upFile(req,res){
    const programa = await Programa.findOne({where:{id_programa:req.body.idPrograma}});
    let namePrograma = `${programa.n_drive_google}.${programa.sigla_programa.trim()}`;
    let nameAlumn = `${req.body.codAlumno}`;

    drive.files.list({parents:[`${ROOT_DIRECTORY}`],q:`name='${namePrograma}' and trashed = false and mimeType = 'application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err1,res1){
        if(err1){
            console.log(err1);
            return res.json({err1:'Algo no salió bien'});
        } else {
            if(res1.data.files.length==0){
                let idFolderProgram = await createFolder(namePrograma,ROOT_DIRECTORY);
                drive.files.list({q:`name='${req.body.anio_ingreso}' and trashed = false and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err2,res2){
                    if(err2){
                        console.log(err2);
                        return res.json({err2:'Algo no salió bien'});
                    } else {
                        if(res2.data.files.length==0){
                            let idFolderIngreso = await createFolder(req.body.anio_ingreso,idFolderProgram);
                            drive.files.list({q:`name contains '${nameAlumn}' and trashed = false and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err3,res3){
                                if(err3){
                                    console.log(err3);
                                    return res.json({err3:'Algo no salió bien'});
                                } else {
                                    if(res3.data.files.length==0){
                                        let idFolderAlumno = await createFolder(nameAlumn,idFolderIngreso);               
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    } else {
                                        let idFolderAlumno = res3.data.files[0].id;
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    }
                                }    
                            });
                        } else {
                            let idFolderIngreso = res2.data.files[0].id;
                            drive.files.list({q:`name contains '${nameAlumn}' and trashed = false and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err3,res3){
                                if(err3){
                                    console.log(err3);
                                    return res.json({err3:'Algo no salió bien'});
                                } else {
                                    if(res3.data.files.length==0){
                                        let idFolderAlumno = await createFolder(nameAlumn,idFolderIngreso);               
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    } else {
                                        let idFolderAlumno = res3.data.files[0].id;
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    }
                                }    
                            });
                        }
                    }    
                });
                

            } else {
                let idFolderProgram = res1.data.files[0].id;
                console.log(idFolderProgram);

                drive.files.list({q:`name='${req.body.anio_ingreso}' and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err2,res2){
                    if(err2){
                        console.log(err2);
                        return res.json({err2:'Algo no salió bien'});
                    } else {
                        if(res2.data.files.length==0){
                            let idFolderIngreso = await createFolder(req.body.anio_ingreso,idFolderProgram);
                            drive.files.list({q:`name contains '${nameAlumn}' and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err3,res3){
                                if(err3){
                                    console.log(err3);
                                    return res.json({err3:'Algo no salió bien'});
                                } else {
                                    if(res3.data.files.length==0){
                                        let idFolderAlumno = await createFolder(nameAlumn,idFolderIngreso);               
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    } else {
                                        let idFolderAlumno = res3.data.files[0].id;
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    }
                                }    
                            });
                        } else {
                            let idFolderIngreso = res2.data.files[0].id;
                            drive.files.list({q:`name contains '${nameAlumn}' and mimeType='application/vnd.google-apps.folder'`,spaces:'drive'}, async function(err3,res3){
                                if(err3){
                                    console.log(err3);
                                    return res.json({err3:'Algo no salió bien'});
                                } else {
                                    if(res3.data.files.length==0){
                                        let idFolderAlumno = await createFolder(nameAlumn,idFolderIngreso);               
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    } else {
                                        let idFolderAlumno = res3.data.files[0].id;
                                        let idNewFile = await createFile(req.body.idRec,req.file.filename,req.file.mimeType,idFolderAlumno);
                                        unlink(path.join(__dirname,'..','uploads','files',req.file.filename));
                                        await Recaudaciones.update({id_drive_google:idNewFile}, {returning: false,where: {numero:req.body.idRec}});
                                        return res.json({idNewFile});
                                    }
                                }    
                            });
                        }
                    }    
                });
            }
        }
    });
    
}

export async function getRecipe(req,res){
    const recaudacion = await Recaudaciones.findOne({where:{numero: req.params.id}});
    return res.json(recaudacion.id_drive_google)
}
