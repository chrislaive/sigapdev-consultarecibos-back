import { Router } from 'express';
import apiFile from './apiFile';

const app = Router();

app.use('/file', apiFile);

export default app;