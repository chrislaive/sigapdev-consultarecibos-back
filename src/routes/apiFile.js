import { Router } from 'express';
import * as fileController from '../controller/apiFile';
import uploadMulterFile from '../middleware/multerFile';
import { file } from 'googleapis/build/src/apis/file';

const app = Router();

app.get('/findrecipe/:id', fileController.getRecipe);
app.post('/upFile', uploadMulterFile, fileController.upFile);
app.post('/editFile', uploadMulterFile, fileController.editFile);
app.post('/deleteFile', fileController.deleteFile);

export default app;