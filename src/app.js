import express from 'express';
import dotenv from 'dotenv';
dotenv.config();
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import routes from './routes';

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json({}));
app.use(cookieParser());
app.use(cors({credentials:false}));

app.use('/api', routes);

app.listen(process.env.PORT, process.env.HOST, function(){
    console.log('Server started on port ' + process.env.PORT)
});