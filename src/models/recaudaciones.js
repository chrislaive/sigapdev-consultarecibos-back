import Sequelize from 'sequelize';
import {sequelize} from '../database';

const recaudaciones = sequelize.define('recaudaciones',{
    id_rec:{
        type: Sequelize.NUMBER,
        allowNull: false,
        primaryKey: true
    },
    numero: Sequelize.STRING,
    id_drive_google: Sequelize.STRING
},{
    timestamps: false,
    freezeTableName: true,
    tableName: 'recaudaciones'
});

export default recaudaciones;