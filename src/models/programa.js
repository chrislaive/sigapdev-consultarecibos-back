import Sequelize from 'sequelize';
import {sequelize} from '../database';

const programa = sequelize.define('programa',{
    id_programa:{
        type: Sequelize.NUMBER,
        allowNull: false,
        primaryKey: true
    },
    sigla_programa: Sequelize.STRING,
    n_drive_google: Sequelize.STRING
},{
    timestamps: false,
    freezeTableName: true,
    tableName: 'programa'
});

export default programa;